# Remote SSH with Visual Studio Code

**Prerequisites**
1. Install Visual Studio Code from https://code.visualstudio.com/ on your host machine. You should already know how to do this.
2. Install VPN on your host machine. Follow the tutorial here https://it.mst.edu/services/vpn/.
3. Install the latest Python from the Microsoft Store

## Install mst-ssh
We're going to need to create a SSH config file and generate SSH keys that we'll be using to connect to a MST virtual machine. To simplify the process, we will install `mst-ssh`. This python script will ask you for your password because the SSH keys are generated locally. The password opens up a temporary connection and copies the public key to the remote virtual machine.

1. Search '`cmd`' in the Windows task bar. Select `Command Prompt`.
2. Type `pip3 install mst-ssh`. Note the warning text. Copy everything between the single quotes.
![](static/install-mst-ssh.gif)
3. Now we need to add what we just copied to our system path. Otherwise Windows won't be able to find our setup script! Search '`path`' in the Windows task bar. Select `Edit the system environment variables`.
4. Click on the `Environment variables...` button. It'll open up a new window. Select `Path` and press `Edit`. Then paste in the directory. If you copied the quotes too, make sure to remove the quotes after pasting ![](static/edit-path.gif)
5. Open up a new command prompt and type `mst-ssh.exe`. If you're not connected to the VPN, it'll prompt you to do so. However, if you're connected and it still asks you, that's because we can't connect to that specific machine. ![](static/setup-keys.gif)
6. Set up as many connections as you want. Make sure at least one ends in `Successfully configured <machine name>`.

## Setting up the SSH extension in VSCode
1. Turn on autosave. This is extremely important as you will not be editing files on your local machine. In the event of disconnect, any unsaved progress will be lost unless they've been saved on the Remote.
![step-1](static/step-1-auto-save.gif)
#
2. Install Microsoft's proprietary **Remote.SSH** extension. 
![step-2](static/step-2-remote-ssh.gif)
#
3. Set lock file location in tmp. By default, a lock file is placed within your `$HOME` directory. However, since MST uses a Distributed File System, we must place the lock file in `/tmp` directory. To do this, go into your settings, search `remote.ssh:tmp` and check the first option.
![step-3](static/step-3-lockfile.gif)
#
4. Connect to VPN if you're not on the campus network. If you don't have OpenVPN installed, follow the directions here https://it.mst.edu/services/vpn/.
![step-4](static/step-5-vpn-connect.gif)
5. Connect to remote machine. Click on the green square on the lower left corner and select `Remote-SSH: Connect Current Window to Host`. Then chose an SSH host. Then select `Linux` as the operating system. Then select `Continue`. Enter your password if you haven't set up passwordless entry. It's a good idea to open up the `details` tab on the lower right. In case you're unsuccessful in connecting, the output will help in troubleshooting what went wrong. Once you see the green box turn into a green rectangle that displays the hostname, you're in!
![step-6](static/step-6-ssh-connect.gif)
